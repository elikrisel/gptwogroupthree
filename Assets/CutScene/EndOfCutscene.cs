using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndOfCutscene : MonoBehaviour
{
    public GameObject skip;
    public GameObject playerActive;
    public GameObject uiActive;
    public float endCS;

    void Start()
    {
        StartCoroutine(EndCS());

    }

    public IEnumerator EndCS()
    {
        yield return new WaitForSeconds(endCS);

        skip.SetActive(false);
        playerActive.SetActive(true);
        uiActive.SetActive(true);
    }


}
