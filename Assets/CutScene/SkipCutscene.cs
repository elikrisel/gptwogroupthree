using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkipCutscene : MonoBehaviour
{
    public GameObject skip;
    public GameObject playerActive;
    public GameObject uiActive;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            skip.SetActive(false);
            playerActive.SetActive(true);
            uiActive.SetActive(true);
        }
    }
}