using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleTalk : MonoBehaviour
{
    public GameObject questionmark;
    
    bool is_player_inside;



    
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            is_player_inside = true;
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && is_player_inside)
        {
            Destroy(questionmark);
            
        }
    }

}
