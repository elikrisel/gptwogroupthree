using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerUILight : MonoBehaviour
{

    public GameObject Light;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Light.SetActive(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Light.SetActive(false);
        }
    }

}
