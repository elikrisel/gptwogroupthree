using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnderwaterFog : MonoBehaviour
{
    public GameObject dirLight;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            RenderSettings.fogDensity = 0.008f;
            dirLight.GetComponent<Light>().shadows = LightShadows.None;
        }

    }
    void OnTriggerExit(Collider other)
    {
          if (other.gameObject.CompareTag("Player"))
         {
             RenderSettings.fogDensity = 0.002f;
             dirLight.GetComponent<Light>().shadows = LightShadows.Soft;
         }
    }
}
