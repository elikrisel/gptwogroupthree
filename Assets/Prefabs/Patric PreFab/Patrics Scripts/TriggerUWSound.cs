using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerUWSound : MonoBehaviour
{
    public AudioSource underwatersound;

    void Start()
    {
        underwatersound = GetComponent<AudioSource>();
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            underwatersound.Play();;
        }
    }
}
