using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerUWMusic : MonoBehaviour
{
    public AudioSource underwatermusic;
    public AudioSource underwaterSound;
    public GameObject windAudio;
    public GameObject VillageAudio;
    public GameObject VillageAudio2;
    public GameObject FireSound;
    public GameObject Snow;
    public GameObject Snow2;
    public GameObject FlashlightUI;
    //public GameObject Shark;

    void Start()
    {
        underwatermusic = GetComponent<AudioSource>();
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            //Turn on
            underwatermusic.Play();
            underwaterSound.Play();
            FlashlightUI.SetActive(true);
            //Shark.SetActive(true);
            //Destroy(Shark, 50f);


            //Turn off
            windAudio.SetActive(false);
            VillageAudio.SetActive(false);
            VillageAudio2.SetActive(false);
            FireSound.SetActive(false);
            Snow.SetActive(false); 
            Snow2.SetActive(false);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
           // Turn off
            underwatermusic.Stop();
            underwaterSound.Stop();
            FlashlightUI.SetActive(false);


            //Turn on
            windAudio.SetActive(true);
            VillageAudio.SetActive(true);
            VillageAudio2.SetActive(true);
            FireSound.SetActive(true);
            Snow.SetActive(true);
        }
    }
}
