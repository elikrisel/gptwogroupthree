using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonAnim : MonoBehaviour
{
    [SerializeField] private Animator myAnimationController;
    public GameObject secondTrigger;

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {

        myAnimationController.SetBool("SkeletonFall", true);
            secondTrigger.SetActive(true);

        }

    }
}
