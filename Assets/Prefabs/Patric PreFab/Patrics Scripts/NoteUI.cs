using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NoteUI : MonoBehaviour
{
    [SerializeField]

    public GameObject _noteImage;
    bool is_player_inside;
    public AudioSource NoteSound;
    public GameObject openRead;
    public GameObject closeRead;



    void Start()
    {
        NoteSound = GetComponent<AudioSource>();
        NoteSound.Stop();
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            is_player_inside = true;
            openRead.SetActive(true);

}
    }
    

    void OnTriggerExit(Collider other)
    {
        is_player_inside = false;
        _noteImage.SetActive(false);
        openRead.SetActive(false);
        closeRead.SetActive(false);
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && is_player_inside)
        {
            openRead.SetActive(!openRead.gameObject.activeSelf);
            closeRead.SetActive(!closeRead.gameObject.activeSelf);
            _noteImage.SetActive(!_noteImage.gameObject.activeSelf);
            NoteSound.Play();
        }
    }

}
