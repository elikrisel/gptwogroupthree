using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveShark : MonoBehaviour
{
    public GameObject shark;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            shark.SetActive(true);
        }
    }
}
