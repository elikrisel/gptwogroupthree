using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyZone : MonoBehaviour
{
    public GameObject roamer;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Vehicle" || other.gameObject.tag == "Player")
        {
            Destroy(roamer);
        }
    }
}
