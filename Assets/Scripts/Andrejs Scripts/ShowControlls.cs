using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowControlls : MonoBehaviour
{

    public GameObject showControlls;

    bool on;

    // Start is called before the first frame update
    void Start()
    {
        showControlls.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("i"))
        {
            if (on == true)
            {
                on = false;
            }

            else
            {
                on = true;
            }
        }

        if (on == true)
        {
            showControlls.SetActive(true);
        }
        else
        {
            showControlls.SetActive(false);
        }
    }
}
