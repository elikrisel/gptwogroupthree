using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MetalHandin : MonoBehaviour
{
    public CollectibleSystem collectible;

    public int reward = 0;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Handin"))
        {

            Destroy(gameObject);

            collectible.metal += reward;

            //Debug.Log("Got Plastic: " + reward);
        }

    }

}
