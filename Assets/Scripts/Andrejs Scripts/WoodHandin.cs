using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodHandin : MonoBehaviour
{
    public CollectibleSystem collectible;

    public int reward = 0;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Handin"))
        {

            Destroy(gameObject, 2);

            collectible.wood += reward;

            //Debug.Log("Got Wood: " + reward);
        }

    }

}
