using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlowShader : MonoBehaviour
{

    public Material glow;
    public Material noGlow;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            transform.GetComponent<Renderer>().material = glow; 
        }

        else
        {
            transform.GetComponent<Renderer>().material = noGlow;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        transform.GetComponent<Renderer>().material = noGlow;
    }
}
