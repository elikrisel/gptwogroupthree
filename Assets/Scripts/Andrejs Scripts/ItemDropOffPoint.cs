using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDropOffPoint : MonoBehaviour
{

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Wood"))
        {

            Destroy(other.gameObject);

        }

        if (other.gameObject.CompareTag("Metal"))
        {

            Destroy(other.gameObject);

        }

        if (other.gameObject.CompareTag("Plastic"))
        {

            Destroy(other.gameObject);

        }
    }
}
