using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderOnEnter : MonoBehaviour
{

    [SerializeField] private Material myMaterial;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            myMaterial.color = Color.green;
        }

        else
        {
            myMaterial.color = Color.red;
        }
    }
}
