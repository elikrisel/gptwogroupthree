using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemHoldingAndrejs : MonoBehaviour
{
    public GameObject playerCube;
    public GameObject npcCube;
    public GameObject staticCube;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("e"))
        {
            if (staticCube.activeSelf == true)
            {
                staticCube.SetActive(false);
                playerCube.SetActive(true);
            }
        }

        if (Input.GetKeyDown("x"))
        {
            if (playerCube.activeSelf == true)
            {
                playerCube.SetActive(false);
                npcCube.SetActive(true);
            }

            else
            {

            }
        }
    }
}
