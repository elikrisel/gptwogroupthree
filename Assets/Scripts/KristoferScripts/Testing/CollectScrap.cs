using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
/// <summary>
/// See CollectMetal script for more information
/// </summary>
public class CollectScrap : MonoBehaviour
{
    //calling pickup test script
    public CollectibleSystem collectible;
    //variable for additional pickup
    [Header("Total value when picking something up")]
    public int addAmount;
    public GameObject pickUpUI;

    void Start()
    {
        pickUpUI.SetActive(false);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            collectible.scrap += addAmount;

            Destroy(gameObject);

            pickUpUI.SetActive(false);

            pickUpUI.SetActive(true);

        }
    }

}
