using System.Collections;
using System.Collections.Generic;
using Isvak.Player;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class BuffAndDebuff : MonoBehaviour
{   //Warm state at default
    public SurvivalState survivalState = SurvivalState.Warm;
    
    //Decrease temperature going down.Can be tweaked in Inspector how fast you want to go down, the lower the slower..
    public float decreasePerMinute;
    //When inside a building it determines wether how fast you want the temperature to go up. Can be tweaked in Inspector.
    public float increaseTimer;
    
    public bool isInside;
    
    
    void Start()
    {   //not in inside is being set to false at default
        isInside = false;
        
        

    }

    void Update()
    {   
        if (!isInside)
        {

            PlayerStats.CurrentTemperature -= Time.deltaTime * decreasePerMinute / 60f;
            //State machine, depending on the state, the movement speed affects the player
            switch (PlayerStats.CurrentTemperature)
            {
                case var _ when (PlayerStats.CurrentTemperature <= 75 && PlayerStats.CurrentTemperature >= 50):
                    
                    survivalState = SurvivalState.Cold;
                    
                    // Slow down player by 10%
                    PlayerStats.speedModifier = 0.9f;
                    break; 
                case var _ when (PlayerStats.CurrentTemperature <= 50 && PlayerStats.CurrentTemperature >= 25):
                    survivalState = SurvivalState.Freezing;
                    
                    // Slow down player by 50%
                    PlayerStats.speedModifier = 0.50f;
                    break;
                case var _ when PlayerStats.CurrentTemperature <= 25:
                    survivalState = SurvivalState.Icecold;
                    
                    // Slow down player by 90%
                    PlayerStats.speedModifier = 0.1f;
                    break;
                

            }

            
            
            if (PlayerStats.CurrentTemperature <= 0)
            {
                PlayerStats.CurrentTemperature = 0;

            }

            

        }
        if (isInside)
        {
            //When inside a building it regenerates back the lost temperature and resets the speedmodifier.        
            PlayerStats.CurrentTemperature += Time.deltaTime * increaseTimer;
            
            
            if (PlayerStats.CurrentTemperature >= 75)
            {
                survivalState = SurvivalState.Warm;
                PlayerStats.speedModifier = 1.0f;


            }

            if (PlayerStats.CurrentTemperature >= PlayerStats.BaseTemperature)
            {

                PlayerStats.CurrentTemperature = PlayerStats.BaseTemperature;

            }
            
        }

    }

    //State machine of current condition of the player

    public enum SurvivalState
    {
        Warm,
        Cold,
        Icecold,
        Freezing,


    }


}

