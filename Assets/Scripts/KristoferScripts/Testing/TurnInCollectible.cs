using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Script that's being a turn in items to the collectible station in the village. Only have metal and plastic reward.
/// </summary>
public class TurnInCollectible : MonoBehaviour
{
    public CollectibleSystem collectible;

    public int reward;

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Item2"))
        {
            
            Destroy(other.gameObject);

           
            collectible.metal += reward;


        }

        if (other.gameObject.CompareTag("Item3"))
        {

            Destroy(other.gameObject);
           
            collectible.plastic += reward;



        }
    }
}
