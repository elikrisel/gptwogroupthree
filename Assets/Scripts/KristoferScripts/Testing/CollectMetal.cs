using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Collectible system in the game. It shows the amount the player have picked up of everything.
/// When the player is colliding with the collectible, it adds on the UI and destroys the object
/// </summary>
public class CollectMetal : MonoBehaviour
{

    public CollectibleSystem collectible;
    
    [Header("Total value when picking something up")]
    public int addAmount;
    //Designer implementation in this and in other "Collect..." scripts    
    public GameObject pickUpUI;

    void Start()
    {
        pickUpUI.SetActive(false);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {   
            collectible.metal += addAmount;
            
            Destroy(gameObject);

            pickUpUI.SetActive(false);

            pickUpUI.SetActive(true);


        }
    }
}
