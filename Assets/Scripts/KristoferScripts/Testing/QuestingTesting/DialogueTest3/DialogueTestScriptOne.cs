using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Isvak.Player;
using Isvak.StateMachine;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
/// <summary>
/// This one is a long and messy script, I'm aware it can be done better and I made several similar attempts that didn't go so well.
/// The bool has been done for testing purposes to track the progress of the dialogue system
/// Many different functions that fill the purpose on depending on which option the player clicks and getting tons of component of texts and converting
/// them to strings.
/// </summary>
public class DialogueTestScriptOne : MonoBehaviour
{   
    [Header("Different canvas gameobjects")]
    #region Canvas
    [SerializeField] private GameObject dialogueOne;
    [SerializeField] private GameObject dialogueTwo;
    [SerializeField] private GameObject questAcceptedHint;
    [SerializeField] private GameObject questCompleted;
    #endregion
    
    [Header("Checking different systems are activated")]
    #region bool components
    public bool inConversation;
    public bool optionOne;
    public bool acceptedQuest;
    public bool completedQuest;
    #endregion
    
    #region CollectibleSystem
    private int questReward;
    public CollectibleSystem collectible;
    #endregion
    
    #region Interactable
    [Header("InteractableDialogueBox attached on NPC")]
    public InteractableDialogue interactable;
    #endregion
    
    #region Cameras

    [SerializeField] private GameObject dialogueCamera;
    [SerializeField] private GameObject playerCamera;
    #endregion
    
    #region DialogueText
    [Header("Dialogue Text in Dialogue 1")]
    public Text nameText;
    public Text descriptionText;
    public Text optionOneText;
    public Text optionTwoText;

    public string nameString;
    public string descriptionString;
    public string optionOneString;
    public string optionTwoString;

    [Header("Dialogue Text in Dialogue 2")]
    
    public Text nameInDialogue2Text;
    public Text descriptionOneText;
    public Text optionOneInFirstText;
    public Text optionTwoInFirstText;
    
    
    public string nameinDialogue2String;
    public string descriptionOneString;
    public string optionOneInFirstString;
    public string optionTwoInFirstString;


    [Header("BackWithoutStuff")] 
    public Text nameInQuestHintText;
    public Text descriptionInQuestHintText;
    
    public string nameInQuestHintString;
    public string descriptionInQuestHintString;

    [Header("BackWithStuff")] 
    public Text nameInCompleteQuest;
    public Text descriptionInCompleteQuestText;
    
    public string nameInCompleteQuestString;
    public string descriptionInCompleteQuestString;
    #endregion
    
    #region Main Methods
    void Start()
    {
        
        dialogueOne.SetActive(false);
        dialogueTwo.SetActive(false);
        questAcceptedHint.SetActive(false);
        questCompleted.SetActive(false);
        inConversation = false;
        //optionOne = false;
        acceptedQuest = false;
    }

   

    public void OnTriggerStay(Collider other)
    {
        //When the item is in the trigger, it destroys the object and you get quest completed and quest rewards, which is 20 of each collectible.
        if (other.gameObject.CompareTag("Item") && !completedQuest)
        {
            Destroy(other.gameObject);
            completedQuest = true;
            
            
            questReward = 50;
            //collectible.scrap += questReward;
            //collectible.metal += questReward;
            //collectible.plastic += questReward;
            collectible.wood += questReward;
            
            CompleteQuest();

        }
        
    }
    #endregion
    
    #region Conversation
    public void FirstConversation()
    {
        PlayerStats.playerIsBusy = true;
        dialogueCamera.SetActive(true);
        playerCamera.SetActive(false);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        
        
        inConversation = true;
        acceptedQuest = false;
        optionOne = false;
        
        dialogueOne.SetActive(true);
        nameText.GetComponent<Text>().text = nameString;
        descriptionText.GetComponent<Text>().text = descriptionString;
        optionOneText.GetComponent<Text>().text = optionOneString;
        optionTwoText.GetComponent<Text>().text = optionTwoString;
      
    }

    public void FirstOption()
    {
        
        optionOne = true;
        
        dialogueTwo.SetActive(true);
        dialogueOne.SetActive(false);
        nameInDialogue2Text.GetComponent<Text>().text = nameinDialogue2String;
        descriptionOneText.GetComponent<Text>().text = descriptionOneString;
        optionOneInFirstText.GetComponent<Text>().text = optionOneInFirstString;
        optionTwoInFirstText.GetComponent<Text>().text = optionTwoInFirstString;




    }

   
    public void Accept()
    {
        dialogueCamera.SetActive(false);
        playerCamera.SetActive(true);
        PlayerStats.playerIsBusy = false;
        inConversation = false;
        
        

        acceptedQuest = true;
        dialogueOne.SetActive(false);
        dialogueTwo.SetActive(false);
        interactable.interact.SetActive(true);
        
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        

    }

    
    
    

    public void Close()
    {
        PlayerStats.playerIsBusy = false;
        inConversation = false;
        
        optionOne = false;
        
        dialogueOne.SetActive(false);
        dialogueTwo.SetActive(false);
        interactable.interact.SetActive(true);        
        
        dialogueCamera.SetActive(false);
        playerCamera.SetActive(true);
        
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        

    }
    #endregion
    
    #region QuestHint
    

    public void QuestHint()
    {
        PlayerStats.playerIsBusy = true;
        nameInQuestHintText.GetComponent<Text>().text = nameInQuestHintString;
        descriptionInQuestHintText.GetComponent<Text>().text = descriptionInQuestHintString;
        
        dialogueCamera.SetActive(true);
        playerCamera.SetActive(false);
        
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        questAcceptedHint.SetActive(true);
            
    }

    public void CloseQuestHint()
    {
        PlayerStats.playerIsBusy = false;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        questAcceptedHint.SetActive(false);
        interactable.interact.SetActive(true);
        
        dialogueCamera.SetActive(false);
        playerCamera.SetActive(true);
        
    }
    #endregion
    
    #region Complete Quest
    public void CompleteQuest()
    {
        PlayerStats.playerIsBusy = true;
        nameInCompleteQuest.GetComponent<Text>().text = nameInCompleteQuestString;
        descriptionInCompleteQuestText.GetComponent<Text>().text = descriptionInCompleteQuestString;
        
        dialogueCamera.SetActive(true);
        playerCamera.SetActive(false);
        
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        
        questCompleted.SetActive(true);
        

    }

    public void CloseComplete()
    {

        PlayerStats.playerIsBusy = false;
        
        acceptedQuest = false;
        optionOne = false;
        completedQuest = false;
        
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        interactable.interact.SetActive(true);
        questCompleted.SetActive(false);
        
        dialogueCamera.SetActive(false);
        playerCamera.SetActive(true);
        
        
    }   
    #endregion
    
    
}
