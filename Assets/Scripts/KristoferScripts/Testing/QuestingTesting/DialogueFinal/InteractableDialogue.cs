using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class InteractableDialogue : MonoBehaviour
{
    public GameObject interact;
    public bool inRangeofDialogue;
    
    public KeyCode interactKey;
    
    public DialogueTestScriptOne dialogue;
    // Different unity event that gets triggered depending on the state of the questline
    public UnityEvent questHint;
    public UnityEvent firstConversation;
    public UnityEvent completedQuest;
    
    void Awake()
    {
        
        interact.SetActive(false);
        

    }
    

    void Update()
    {
        if (inRangeofDialogue)
        {
            if (Input.GetKeyDown(interactKey))
            {
                //When the player is in range and the player press the interact key, it loads the script
                
                interact.SetActive(false);
                //inRangeofDialogue = false;
                
                if (dialogue.acceptedQuest)
                {

                    questHint.Invoke();

                }
                else if (dialogue.completedQuest)
                {
                    completedQuest.Invoke();
                      
                    

                }
                else
                {
                    firstConversation.Invoke();
                }
                
                



            }

            
            
                        
        }
               
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            //When the player enters the trigger, it's being set to true
            inRangeofDialogue = true;
            interact.SetActive(true);

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {

            inRangeofDialogue = false;
            interact.SetActive(false);

        }
    }

}
