using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Interact Event script. It's being set on InteractableBox prefab to set a keycode when entering a trigger and invoke the event.
/// </summary>
public class Interactable : MonoBehaviour
{

    public GameObject interact;
    public bool inRange;
    public KeyCode interactKey;
    public UnityEvent interactAction;
    
    void Awake()
    {
        
        interact.SetActive(false);
        

    }

    void Update()
    {
        if (inRange)
        {
            if (Input.GetKeyDown(interactKey))
            {
                //When the player is in range and the player press the interact key, it loads the script
                interactAction.Invoke();
                interact.SetActive(false);
                //inRange = false;
                    
            }
            
                        
        }
               
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            //When the player enters the trigger, it's being set to true
            inRange = true;
            interact.SetActive(true);

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {

            inRange = false;
            interact.SetActive(false);

        }
    }

    
    
}
