using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueOneLiner : MonoBehaviour
{

    [SerializeField] private GameObject dialogue;
    [Header("NPC Name")]
    public Text nameText;
    public string nameString;

    [Header("Dialogue")]
    public Text dialogueText;
    public string dialogueString;
    
    [Header("InteractableBox attached on NPC")]
    public Interactable interactable;

    [Header("Cameras")] 
    [SerializeField] private GameObject playerCamera;

    [SerializeField] private GameObject dialogueCamera;
    void Start()
    {
        dialogue.SetActive(false);
        nameText.GetComponent<Text>();
        dialogueText.GetComponent<Text>();

    }



     public void OneLiner()
     {
         PlayerStats.playerIsBusy = true; 
         Cursor.visible = true;
         Cursor.lockState = CursorLockMode.None;
         
         dialogue.SetActive(true);
         dialogueCamera.SetActive(true);
         playerCamera.SetActive(false);
         
         
         nameText.text = nameString;
         dialogueText.text = dialogueString;

     }
    public void CloseOneLiner()
    {
        PlayerStats.playerIsBusy = false;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        
        
        dialogue.SetActive(false);
        
        playerCamera.SetActive(true);
        dialogueCamera.SetActive(false);
        interactable.interact.SetActive(true);

    }

}
