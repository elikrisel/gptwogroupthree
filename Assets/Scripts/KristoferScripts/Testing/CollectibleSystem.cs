using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


//Collectible system, it's being tied up with Collect Scrap/Metal/Plastic/Wood that tracks on how much the player has.
public class CollectibleSystem : MonoBehaviour
{
    //Text gameobjects - "Plastic,Wood, Metal, Scrap"
    
    public Text scrapText;
    public Text woodText;
    public Text plasticText;
    public Text metalText;
    
    //collectible variables - "plastic, wood, metal, scrap"
    public int scrap;
    public int wood;
    public int plastic;
    public int metal;

    void Start()
    {

        scrapText.GetComponent<Text>();
        woodText.GetComponent<Text>();
        plasticText.GetComponent<Text>();
        metalText.GetComponent<Text>();


    }
    
    void Update()
    {

        //Getting text component for all the collectibles and show the int variable as a string.
        scrapText.text = scrap.ToString();
        woodText.text = wood.ToString();
        plasticText.text = plastic.ToString();
        metalText.text = metal.ToString();
        
    }
}
