using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Safezone : MonoBehaviour
{

    public BuffAndDebuff player;
   //Getting 10 seconds buff when player is getting inside
    private float duration = 10f;
    public bool getBuff;
    
    void Start()
    {
        getBuff = false;

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player") || !getBuff)
        {
            
            player.isInside = true;            
        
            StartCoroutine(Buff());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            
            player.isInside = false;
            
            
            if (getBuff)
            {
                PlayerStats.CurrentTemperature += duration;

            }
        }
    }

    IEnumerator Buff()
    {
        //Buff activated after 5 seconds when inside a building
        yield return new WaitForSeconds(5);
        getBuff = true;
        
        

    }

    
       
}
