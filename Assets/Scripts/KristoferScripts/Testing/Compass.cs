using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Prototype Compass I made on my free time. Will see if this will be implemented in the game.
/// </summary>
public class Compass : MonoBehaviour
{

   
    //Compass image
    public RawImage compassImage;
    //Player
    public Transform player;



    void Update()
    {

        compassImage.uvRect = new Rect(player.localEulerAngles.y / 360f, 0f, 1f, 1f);

        
        
    }


}
