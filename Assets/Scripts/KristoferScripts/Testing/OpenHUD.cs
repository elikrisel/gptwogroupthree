using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Prototype HUD I made at my freetime. Will see if this will be in the final version.
/// </summary>
public class OpenHUD : MonoBehaviour
{
    //HUD GameObject that's in Canvas
    [SerializeField] private GameObject hud;
        
    [SerializeField] private bool isPressed;
    
    
    

    
    void Start()
    {
        //bool is set to false as default
        isPressed = false;
    

    }
    
    void Update()
    {
        //HUD button is added in Input Manager as "H"
        
        if (!isPressed && Input.GetButtonDown("HUD"))
        {
            isPressed = true;
            Open();

        }
        else if (isPressed && Input.GetButtonDown("HUD"))
        {
            Close();
            isPressed = false;
        }
        
        
    }
    //When the button is pressed the game object is being setactive to true
    public void Open()
    {
        
        hud.SetActive(true);
        
        
    }
    //when closing the hud it's being set to false
    public void Close()
    {
        
        hud.SetActive(false);
        
        
    }
    
    
}
