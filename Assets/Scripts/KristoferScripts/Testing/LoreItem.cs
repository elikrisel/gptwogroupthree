using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script sits on the Player so we can destroy the object with the tag one by one
/// instead of either destroying all at once or destroying the functionality on Close Button
/// </summary>
public class LoreItem : MonoBehaviour
{

    [SerializeField] private GameObject loreCanvas;

    public Text descriptionText;
    public string descriptionString;

    public Text loreText;
    public string loreString;
    
    public string[] tags;

    
    
    void Start()
    {
        loreCanvas.SetActive(false);
        tags = new[] {"LoreItem"};
        
        descriptionText.GetComponent<Text>();
        loreText.GetComponent<Text>();

    }


    public void PickUp()
    {
        if (tags.Any())
        {   
            gameObject.SetActive(false);
            Time.timeScale = 0;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            
            loreCanvas.SetActive(true);
            descriptionText.text = descriptionString;
            loreText.text = loreString;
    
            
        }
        
        
        
        
    }
        


    public void Close()
    {
        
        Time.timeScale = 1;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        
        loreCanvas.SetActive(false);
        


    }


}
