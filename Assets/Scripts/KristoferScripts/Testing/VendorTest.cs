using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Vendor where you turn in collectibles to upgrade your stats, wether you want to increase your health, temperature and oxygen.
/// We also have the feature to complete the demo by turning in Relics.
/// </summary>
public class VendorTest : MonoBehaviour
{
    //Calling pickuptest script
    [Header("CollectibleSystem")]
    public CollectibleSystem collectible;
    [Header("InteractableBox on ShopExperiment")]
    public Interactable interactShop;
    
    [Header("Canvas GameObjects")]
    [SerializeField] private GameObject shopWindow;
    [SerializeField] private GameObject upgradeWindow;

    [SerializeField] private GameObject gotUpgrade;
    [SerializeField] private GameObject cantUpgrade;
    [SerializeField] private GameObject thanksForPlaying;
    
    [Header("Cameras")]    
    [SerializeField] private GameObject shopCamera;
    [SerializeField] private GameObject playerCamera;

    
    //player stats increase
    private float upgrade = 2f;

    
    
    void Start()
    {
        
        collectible = FindObjectOfType<CollectibleSystem>();
        //Can upgrade is set to false as default
        
        shopWindow.SetActive(false);
        


        

    }
    
    public void OpenShop()
    {
        
        
        PlayerStats.playerIsBusy = true; 
        shopWindow.SetActive(true);
        shopCamera.SetActive(true);
        playerCamera.SetActive(false);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        

        
    }

    
    //Resuming time when closing the shop
    public void Close()
    {
        
        //Hiding Cursor
        PlayerStats.playerIsBusy = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        
        
        shopCamera.SetActive(false);
        playerCamera.SetActive(true);
        
        shopWindow.SetActive(false);
        upgradeWindow.SetActive(false);
        interactShop.interact.SetActive(true);
        

    }

    public void Upgrade()
    {
        
        
        upgradeWindow.SetActive(true);
        shopWindow.SetActive(false);
        
        
        
        
        
        
        
        
    }

    public void GoBack()
    {
        
        
        upgradeWindow.SetActive(false);
        shopWindow.SetActive(true);
        

        
        
    }
    //When the plauer meant certain conditions and have collected the correct amount. He should be able to upgrade
    public void Temperature()
    {
        if (collectible.plastic >= 100 )
        {
            
            PlayerStats.BaseTemperature *= upgrade;
            PlayerStats.CurrentTemperature = ((int)PlayerStats.BaseTemperature);
            collectible.plastic -= 100;
            
            
            gotUpgrade.SetActive(true);
            
        }
        else
        {
          cantUpgrade.SetActive(true);
          
        }
        
        

    }

    public void Oxygen()
    {

        if (collectible.wood >= 100)
        {
            
            PlayerStats.BaseOxygen *= upgrade;
            PlayerStats.CurrentOxygen = ((int)PlayerStats.BaseOxygen);
            collectible.wood -= 100;
            gotUpgrade.SetActive(true);
            
        }
        else
        {
            cantUpgrade.SetActive(true);
            

        }

                

                


    }

    public void Health()
    {

        if (collectible.metal >= 100)
        {
            PlayerStats.BaseHealth *= upgrade;
            PlayerStats.CurrentHealth = ((int)PlayerStats.BaseHealth);
            collectible.metal -= 100;
            
            gotUpgrade.SetActive(true);

        }
        else
        {
            cantUpgrade.SetActive(true);
            
        }
        
        
            


        
        
        
        
        

    }
    //This was meant for the beta test when player is turning in 15 scrap, the player completes the demo
    public void Scrap()
    {
        if (collectible.scrap >= 15)
        {
            

            thanksForPlaying.SetActive(true);
            PlayerStats.playerIsBusy = false;


        }
        else
        {
            //Can't complete quest
            cantUpgrade.SetActive(true);
        }
        
        
    }

    public void GoToMainMenu()
    {
        thanksForPlaying.SetActive(false);
        SceneManager.LoadScene(0);
        PlayerStats.playerIsBusy = false;



    }
    
    
    
}
