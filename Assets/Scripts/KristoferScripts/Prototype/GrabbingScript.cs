using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script was made for the prototype, pretty much got scrapped after having problems with physics and also filled no other purposes
///When having problems with Dialogue System. And for that reason, I wish not to be reviewed on this one.
/// 
/// </summary>
public class GrabbingScript : MonoBehaviour
{   [Header("Player Camera")]
    [SerializeField] private Camera playerCamera;
    //Empty Gameobject attached to the Player
    [Header("Parent GameObject inside Main Camera")]
    [SerializeField] private Transform hands;
    
    
    
    //Reference to pickup script
    private PickableItem pickedItem;
    
    #region Update Method
    
    
    void Update()
    {
        //In this case I created a new Input in Input Manager when pressing the E button.
        //Make sure to do the same when using this script.
        if (Input.GetButtonDown("PickUp"))
        {

            if (pickedItem)
            {
                // if a player have already picked up an item, you drop that item to pick up a new one
                // For instance, you have a treasure with low value and find a treasure with more value
                // you drop the one with less value and pick the one with more. 
                DropItem(pickedItem);
                            
            }
            else
            {
                //creating a ray variable and assigning it to the camera component
                var ray = playerCamera.ViewportPointToRay(Vector3.one * 0.5f);
                // new Raycast variable
                RaycastHit hit;
                //find the object to pick up
                if (Physics.Raycast(ray, out hit, 2f))
                {
                    //pickable variable that's getting the PickableItem Script, I'm aware GetComponent is a no go in Update. But needed to make an exception.
                    var pickable = hit.transform.GetComponent<PickableItem>();

                    // if there's a pickable item
                    if (pickable)
                    {
                        //pick it up
                        PickItem(pickable);
                        //Spawning NPC when object is being picked up
    
                    }


                }

            }
            
        }
        #endregion
        
    }
    #region PickItem
    public void PickItem(PickableItem item)
    {   //assigning PickableItem script to the reference
        pickedItem = item;

        //setting isKinematic to true and use gravity to false when the object is being picked up 
        item.Rb.isKinematic = true;
        item.Rb.useGravity = false;
        //setting the velocities to zero
        item.Rb.velocity = Vector3.zero;
        item.Rb.angularVelocity = Vector3.zero;
        
        //assigning the item to player's hands
        item.transform.SetParent(hands);
        
        //setting the position and rotation to zero
        item.transform.localPosition = Vector3.zero;
        item.transform.localEulerAngles = Vector3.zero;


    }
    #endregion
    
    #region Drop Item
    //Assigning Drop Item to PickableItem script and adding reference
    public void DropItem(PickableItem item)
    {
        // setting the reference to null
        pickedItem = null;
        
        
        //setting the parent to null
        item.transform.SetParent(null);
    
        //enabling useGravity when the item is dropped and setting isKinematic to false
        item.Rb.isKinematic = false;
        item.Rb.useGravity = true;
        //Giving the object a minor force when dropping the item
        item.Rb.AddForce(item.transform.forward * 2, ForceMode.VelocityChange);
        

    }
    #endregion


    
    
}
