using System.Collections;
using System.Collections.Generic;
using Isvak.Player;
using Isvak.StateMachine;
using UnityEngine;

/// <summary>
/// Simple and straight forward Flashlight script with the purpose of using a spotlight when being underwater.
/// Can activate and deactivated. Shuts off when getting out of water
/// </summary>
public class FlashlightTest : MonoBehaviour
{
   public GameObject flashLight;
   public bool lightActive;
   public bool isPressed = false;
   public PlayerHandler _playerHandler;
   void Start()
   {
      
      flashLight.SetActive(false);
      


   }

   void Update()
   {
      
      if (_playerHandler.GetState.ToString() == "Isvak.StateMachine.Swim")
      {

         isPressed = true;
               
         
         if (Input.GetKeyDown(KeyCode.F) && isPressed)
         {

            lightActive = !lightActive;
            flashLight.SetActive(lightActive);
         }

         
         
      }
      else
      {
         isPressed = false;
         lightActive = false;
         flashLight.SetActive(false);
      }
      
      
   }

}
