using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script that's attached to the object that's being grabbed. This is simply made as reference to GrabbingScript and just calling Rigidbody.
/// </summary>
[RequireComponent(typeof(Rigidbody))]
public class PickableItem : MonoBehaviour
{
    //adding a rigidbody automatically to the gameobject when attaching the script
    private Rigidbody rb;
    public Rigidbody Rb => rb;

    
    void Awake()
    {
        //Rigidbody Reference
        rb = GetComponent<Rigidbody>();


    }


}
