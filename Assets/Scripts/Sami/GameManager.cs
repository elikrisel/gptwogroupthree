using System;
using System.Collections;
using System.Collections.Generic;
using Isvak.Player.Settings;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    public static GameManager Instance => instance;


    [SerializeField] private PlayerSettings playerSettings;
    public PlayerSettings PlayerSettings => playerSettings;
    
    [Header("Game settings")]
    [SerializeField] private bool lockAndHideCursor;

	[Header("Scenes")]
	[SerializeField] private string restartToScene = "GameMenu";
    
    
    private void Awake()
    {
        instance = this;
        LockAndHideCursor();
    }

    private void LockAndHideCursor()
    {
        if (lockAndHideCursor)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }
    

	public void RestartGame()
	{
		Time.timeScale = 1.0f;

		PlayerStats.ResetValues();

		if (restartToScene == "") restartToScene = "GameMenu";

		SceneManager.LoadScene(restartToScene);
	}
    
    
}
