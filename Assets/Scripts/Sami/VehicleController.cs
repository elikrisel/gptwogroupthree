using Isvak.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleController : MonoBehaviour
{
	private float heightAboveGround = 1.0f;
	private Vector3 position;

	public GameObject model;
	private float rotationAmount;
	private float speed = 20.0f;
	private Vector3 forwardDirection;

	public Transform player;
	public Transform seat;

	private void Awake()
	{
		//player = GameObject.FindGameObjectWithTag("Player").transform;
	}

	public void Move(Vector3 delta)
	{
		RaycastHit hit = new RaycastHit();
		if (Physics.Raycast (transform.position, -Vector3.up, out hit, 2.5f))
		{
			position = transform.position;
			float distance = hit.distance;
			transform.position = new Vector3(position.x, (position.y - distance) + heightAboveGround, position.z);
			transform.up -= (transform.up - hit.normal) * 0.1f;
			
			Debug.DrawRay(position, -Vector3.up * 1.5f, Color.yellow);
		}
		
		rotationAmount = delta.x * 120.0f;
		rotationAmount *= Time.deltaTime;
	    model.transform.Rotate (0.0f, rotationAmount, 0.0f);
		transform.Rotate(0.0f, rotationAmount, 0.0f);

		forwardDirection = model.transform.forward * delta.z;
		transform.position += forwardDirection * Time.deltaTime * speed;

		player.position = seat.position;
	}
}
