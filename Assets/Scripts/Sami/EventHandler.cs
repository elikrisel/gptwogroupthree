using System.Collections;
using System.Collections.Generic;
using Isvak.StateMachine;
using UnityEngine;
using UnityEngine.Events;

public static class EventHandler
{
    #region Actions
    
    public static event UnityAction<Vector3> Movement;
    public static event UnityAction Jump;
    public static event UnityAction Run;
	public static event UnityAction Interact;

    #endregion

    public static void OnMovement(Vector3 moveValue) => Movement?.Invoke(moveValue);
    public static void OnJump() => Jump?.Invoke();
    public static void OnRun() => Run?.Invoke();
	public static void OnInteract() => Interact?.Invoke();
}