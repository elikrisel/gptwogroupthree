using System.Collections;
using System.Collections.Generic;
using Isvak.Player;
using UnityEngine;

namespace Isvak.StateMachine
{
    public class Dive : State
    {
        #region Variables

        private Vector3 _delta;

        #endregion
        
        public Dive(PlayerHandler playerHandler) : base(playerHandler)
        {
            
        }
        
        public override IEnumerator Start()
        {
            Debug.Log("Diving");
            return base.Start();
        }
        
        public override IEnumerator Move(Vector3 delta)
        {
            _delta = delta;
            return base.Move(delta);
        }

        public override void OnFixedUpdate()
        {
            Transform cam = PlayerHandler.MainCamera.transform;
            
            Vector3 targetVelocity = cam.forward * _delta.z + cam.right * _delta.x;
            targetVelocity = targetVelocity * PlayerHandler.Speed;
            /*if (HeadIsAboveWater())
            {
                targetVelocity.y = Mathf.Clamp(targetVelocity.y, targetVelocity.y, 0.0f);   
            }*/

            PlayerHandler.Rigidbody.MovePosition(PlayerHandler.gameObject.transform.position + targetVelocity * 1.0f * Time.deltaTime);
            
            HeadIsAboveWater();
        }

        private void HeadIsAboveWater()
        {
            RaycastHit hit;
            if (Physics.Raycast(PlayerHandler.head.position, -Vector3.up, out hit, PlayerHandler.headCollider.bounds.size.y + 0.1f))
            {
                if (hit.collider.gameObject.layer == 4)
                {
                    PlayerHandler.SetState(new Swim(PlayerHandler));
                }
            }
        }
    }
}