using System.Collections;
using System.Collections.Generic;
using Isvak.Player;
using UnityEngine;

namespace Isvak.StateMachine
{
    public class Vehicle : State
    {
        #region Variables

        #endregion
        
        public Vehicle(PlayerHandler playerHandler) : base(playerHandler)
        {
            
        }
        
        public override IEnumerator Start()
        {
            Debug.Log("Vehicle");
            return base.Start();
        }

		public override void Interact()
		{
			PlayerHandler.ResetPlayer();
		}

		public override void OnFixedUpdate()
        {
			PlayerHandler.vehicle.Move(PlayerHandler.MovementDelta);
		}
    }
}