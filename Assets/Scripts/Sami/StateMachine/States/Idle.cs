using System.Collections;
using System.Collections.Generic;
using Isvak.Player;
using UnityEngine;

namespace Isvak.StateMachine
{
    public class Idle : State
    {
        public Idle(PlayerHandler playerHandler) : base(playerHandler)
        {
            
        }

        public override IEnumerator Start()
        {
            Debug.Log("Idle");
            return base.Start();
        }

        public override IEnumerator Move(Vector3 delta)
        {
            PlayerHandler.SetState(new Walk(PlayerHandler));
            return base.Move(delta);
        }

        public override IEnumerator Jump()
        {
            Debug.Log("Jump!");
            PlayerHandler.SetState(new Jump(PlayerHandler));
            return base.Jump();
        }
    }
}