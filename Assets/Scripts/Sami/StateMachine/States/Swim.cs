using System.Collections;
using System.Collections.Generic;
using Isvak.Player;
using UnityEngine;

namespace Isvak.StateMachine
{
    public class Swim : State
    {
		#region Variables

		float speed = 40.0f;

        #endregion
        
        public Swim(PlayerHandler playerHandler) : base(playerHandler)
        {
            
        }
        
        public override IEnumerator Start()
        {
            Debug.Log("Swimming");
            return base.Start();
        }
        
        public override void OnFixedUpdate()
        {
            Transform cam = PlayerHandler.MainCamera.transform;
            
            Vector3 targetVelocity = cam.forward * PlayerHandler.MovementDelta.z + cam.right * PlayerHandler.MovementDelta.x;
			
			if (Input.GetKey(KeyCode.Space))
				targetVelocity.y += speed * Time.deltaTime;
			
			if (Input.GetKey(KeyCode.LeftControl)) 
				targetVelocity.y -= speed * Time.deltaTime;

			targetVelocity = targetVelocity * PlayerHandler.Speed;
            //The beautiful poetry below was constructed by Rasmus (with great aid from Kristofer<3)
            //if (Input.GetKey(KeyCode.RightControl))
            if (!Input.anyKey)
                PlayerHandler.Rigidbody.velocity = Vector3.zero;
            //The code above is the POETRY, so beautiful
            PlayerHandler.Rigidbody.MovePosition(PlayerHandler.gameObject.transform.position + targetVelocity * 1.0f * Time.deltaTime);

        }
    }
}