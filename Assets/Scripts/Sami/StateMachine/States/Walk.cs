using System.Collections;
using System.Collections.Generic;
using Isvak.Player;
using UnityEngine;

namespace Isvak.StateMachine
{
    public class Walk : State
    {
        #region Variables

        private float runModifier = 0.0f;

        #endregion
        
        public Walk(PlayerHandler playerHandler) : base(playerHandler)
        {
            runModifier = playerHandler.Speed;
        }
        
        public override IEnumerator Start()
        {
            Debug.Log("Walking");
            return base.Start();
        }

        public override IEnumerator Jump()
        {
            Debug.Log("Jump!");
            PlayerHandler.SetState(new Jump(PlayerHandler));
            return base.Jump();
        }
        
        public override IEnumerator Run(float value)
        {
            runModifier = PlayerHandler.Speed - (PlayerHandler.RunModifier * value);

            return base.Run(value);
        }

        public override void OnFixedUpdate()
        {
            Transform cam = PlayerHandler.MainCamera.transform;
            if (PlayerStats.playerIsBusy)
            {
                return;
            }
            Vector3 targetVelocity = cam.forward * PlayerHandler.MovementDelta.z + cam.right * PlayerHandler.MovementDelta.x;
            targetVelocity = Vector3.ProjectOnPlane(targetVelocity, Vector3.up).normalized;
            targetVelocity = targetVelocity * (runModifier * PlayerStats.speedModifier);

            Vector3 velocity = PlayerHandler.Rigidbody.velocity;
            Vector3 velocityChange = (targetVelocity - velocity);
            velocityChange.x = Mathf.Clamp(velocityChange.x, -PlayerHandler.MAXVelocityChange, PlayerHandler.MAXVelocityChange);
            velocityChange.z = Mathf.Clamp(velocityChange.z, -PlayerHandler.MAXVelocityChange, PlayerHandler.MAXVelocityChange);
            velocityChange.y = 0;

            PlayerHandler.Rigidbody.AddForce(velocityChange, ForceMode.VelocityChange);
            PlayerHandler.Rigidbody.AddForce(new Vector3(0, -10.0f * PlayerHandler.Rigidbody.mass, 0));
            
            CheckForSteps();
        }

        private void CheckForSteps()
        {
            foreach (Vector3 direction in PlayerHandler.directions)
            {
                RaycastHit hit;
                if (Physics.Raycast(PlayerHandler.stepCheck.transform.position, direction, out hit, PlayerHandler.playerWidth + 0.05f))
                {
                    RaycastHit hitUp;
                    if (!Physics.Raycast(PlayerHandler.stepCheckCopy.transform.position, direction, out hitUp, PlayerHandler.playerWidth + 0.1f))
                    {
                        PlayerHandler.Rigidbody.position -= new Vector3(0f, -GameManager.Instance.PlayerSettings.StepSmooth * Time.deltaTime, 0f);
                    }
                }
            }
        }
    }
}