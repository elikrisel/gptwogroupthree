using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine.Utility;
using Isvak.Player;
using UnityEngine;

namespace Isvak.StateMachine
{
    public class Climb : State
    {
        #region Variables

        #endregion
        
        public Climb(PlayerHandler playerHandler) : base(playerHandler)
        {
            
        }
        
        public override void OnFixedUpdate()
        {

            Vector3 targetPosition = PlayerHandler.currentLadder.Find("EndPosition").transform.position;

            /*Bounds ladderBounds = PlayerHandler.currentLadder.GetComponent<BoxCollider>().bounds;
             
            Vector3 offset = ladderBounds.center;
            float height = ladderBounds.size.y;

            float top = (height / 2) + offset.y;

            Debug.Log("THE POSITION " + top);
            
             float width = ladderBounds.size.x;
 
             Vector3 targetPosition = PlayerHandler.currentLadder.transform.position;
             targetPosition.z -= width / 2;
             targetPosition.y = top;
             targetPosition.y += 1.0f;
             */
             float step = 2.0f * Time.deltaTime;
             PlayerHandler.transform.position = Vector3.MoveTowards(PlayerHandler.transform.position, targetPosition, step);
             
             Debug.Log(PlayerHandler.transform.position.y + " and " + targetPosition.y);
             
             if (PlayerHandler.transform.position.y >= targetPosition.y)
             {
                 PlayerHandler.currentLadder = null;
                 PlayerHandler.ResetPlayer();
             }
             
        }
    }
}