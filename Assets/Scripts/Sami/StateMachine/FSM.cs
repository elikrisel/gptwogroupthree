using System;
using UnityEngine;

namespace Isvak.StateMachine
{
    public abstract class FSM : MonoBehaviour
    {
        protected State State;
        public State GetState => State;

        public void SetState(State state)
        {
            State = state;
            StartCoroutine(State.Start());
        }
    }
   
}