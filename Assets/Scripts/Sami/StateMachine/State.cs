using System.Collections;
using Isvak.Player;
using UnityEngine;

namespace Isvak.StateMachine
{
    public abstract class State
    {
        protected PlayerHandler PlayerHandler;
        
        protected State(PlayerHandler playerHandler) => PlayerHandler = playerHandler;

        public virtual IEnumerator Start()
        {
            yield break;
        }

        public virtual IEnumerator Jump()
        {
            yield break;
        }

		public virtual void Interact()
		{

		}
        
        public virtual IEnumerator Move(Vector3 delta = default(Vector3))
        {
            yield break;
        }

        public virtual IEnumerator Run(float value = default(float))
        {
            yield break;
        }

        public virtual IEnumerator Swim()
        {
            yield break;
        }

        public virtual void OnFixedUpdate()
        {
            
        }
    }
}