using System;
using System.Collections;
using System.Collections.Generic;
using Isvak.Player;
using UnityEngine;

namespace Player.Actions
{
    [RequireComponent(typeof(Rigidbody))]
    public class Jump : MonoBehaviour
    {
        #region Variables

        private PlayerHandler _playerHandler;
        
        #endregion
        
        private void OnEnable() => EventHandler.Jump += EventHandlerOnJump;

        private void OnDisable() => EventHandler.Jump -= EventHandlerOnJump;

        private void Awake()
        {
            _playerHandler = GetComponent<PlayerHandler>();
        }

        private void EventHandlerOnJump()
        {
            if (IsGrounded())
            {
                _playerHandler.Rigidbody.AddForce(transform.up * GameManager.Instance.PlayerSettings.JumpForce, ForceMode.VelocityChange);
            }
        }
        
        private bool IsGrounded () => Physics.Raycast(transform.position, -Vector3.up, _playerHandler.DistanceToGround + 0.1f);
    }
}