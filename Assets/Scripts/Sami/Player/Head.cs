using System;
using System.Collections;
using System.Collections.Generic;
using Isvak.Player;
using Isvak.StateMachine;
using UnityEngine;
using TMPro;

public class Head : MonoBehaviour
{
    #region Variables

    public Transform player;
    private PlayerHandler _playerHandler;
    private Rigidbody _rigidbody;

    private bool isSwimming = false;
    private bool isWalking = false;

    public float distanceAboveWater;
    
//    [SerializeField] private float decreasePerMinute = 60f;

    [SerializeField] private TextMeshProUGUI oxygenLevels;

	#endregion
	public GameObject _noteImage;

    private void Awake() => _playerHandler = GetComponentInParent<PlayerHandler>();

    private void Start()
    {
        _rigidbody = player.GetComponent<PlayerHandler>().Rigidbody;
		if (distanceAboveWater == 0.0f)
			distanceAboveWater = 0.5f;
    }

	private void OnTriggerEnter(Collider other)
	{
		if (Input.GetKeyDown(KeyCode.E))
		{
			_noteImage.SetActive(!_noteImage.activeSelf);
			//NoteSound.Play();
		}
	}

	private void Update()
    {
		if (_playerHandler.GetState.ToString() != "Isvak.StateMachine.Climb")
        {
            if (transform.position.y <= distanceAboveWater)
            {
                if (!isSwimming)
                {
                    isSwimming = true;
                    isWalking = false;
                    
                    _rigidbody.useGravity = false;
                    _rigidbody.velocity = Vector3.zero;
                    _rigidbody.angularVelocity = Vector3.zero;
                        
                    _playerHandler.SetState(new Swim(_playerHandler));   
                }
            }
            else
            {
                if (!isWalking)
                {
                    isWalking = true;
                    isSwimming = false;
                    _rigidbody.useGravity = true;

                    _playerHandler.SetState(new Walk(_playerHandler));   
                }
            }   
        }

        if (transform.position.y < -1.0f)
        {
            if (PlayerStats.CurrentOxygen > 0.0f)
            {
                PlayerStats.CurrentOxygen -= Time.deltaTime * 2.65f;
            }
            else
            {
                PlayerStats.CurrentHealth -= Time.deltaTime * 2.65f;
            }
        }
        else
        {
            PlayerStats.CurrentOxygen += Time.deltaTime * 15.0f;
        }
        
        PlayerStats.CurrentOxygen = Mathf.Clamp(PlayerStats.CurrentOxygen, 0.0f, PlayerStats.BaseOxygen);
        
        oxygenLevels.text = $"Oxygen: {PlayerStats.CurrentOxygen.ToString("F0")} \nHealth: {PlayerStats.CurrentHealth.ToString("F0")}";
    }
}
