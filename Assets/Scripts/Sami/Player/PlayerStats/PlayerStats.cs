using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerStats
{
    /*
     * Speed modifier for buff
    */
    public static float speedModifier = 1.0f;

    public static bool playerIsBusy = false;
    /*
     * Health
     */
    public static float BaseHealth = 100.0f;
    public static float CurrentHealth = BaseHealth;
    
    /*
     * Oxygen
     */
    public static float BaseOxygen = 100.0f;
    public static float CurrentOxygen = BaseOxygen;

    public static float BaseTemperature = 100.0f;
    public static float CurrentTemperature = BaseTemperature;

	public static void ResetValues()
	{
		speedModifier = 1.0f;
		
		BaseHealth = 100.0f;
		CurrentHealth = BaseHealth;
		
		BaseOxygen = 100.0f;
		CurrentOxygen = BaseOxygen;

		BaseTemperature = 100.0f;
		CurrentTemperature = BaseTemperature;
	}
}
