using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileInputListener : MonoBehaviour
{
	public GameObject flashlight;
	public GameObject watchObject;
	private bool watchIsActive;

	public void TriggerWatch()
	{
		watchIsActive = !watchIsActive;
		watchObject.SetActive(watchIsActive);
	}
}
