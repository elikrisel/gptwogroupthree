using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using Isvak.Player;
using Isvak.StateMachine;
using TMPro;
using UnityEngine;

public class PlayerInputListener : MonoBehaviour
{
    private PlayerHandler _playerHandler;
    
    private PlayerInput _playerInput;
    private PlayerInput PlayerInput
    {
        get
        {
            if (_playerInput != null) return _playerInput;
            return _playerInput = new PlayerInput();
        }
    }
    
    private void OnEnable()
    {
        PlayerInput.Gameplay.Enable();
        PlayerInput.Gameplay.Movement.started += ctx => OnMove(ctx.ReadValue<Vector2>().normalized);
        PlayerInput.Gameplay.Movement.performed += ctx => OnMove(ctx.ReadValue<Vector2>().normalized);

        PlayerInput.Gameplay.Jump.started += _ => OnJump();
        
        PlayerInput.Gameplay.Run.performed += ctx => OnRun(ctx.ReadValue<float>());
        PlayerInput.Gameplay.Run.canceled += ctx => OnRun(ctx.ReadValue<float>());

		PlayerInput.Gameplay.Interact.started += _ => OnInteract();
    }
    
    private void OnDisable() => PlayerInput.Gameplay.Disable();

    private void Awake()
    {
        _playerHandler = GetComponent<PlayerHandler>();
    }
    
    // private void OnMove(Vector2 delta) => StartCoroutine(_playerHandler.GetState.Move(new Vector3(delta.x, 0.0f, delta.y)));
    private void OnMove(Vector2 delta) => EventHandler.OnMovement(new Vector3(delta.x, 0.0f, delta.y));
    
    private void OnJump() => EventHandler.OnJump();

    private void OnRun(float value) => StartCoroutine(_playerHandler.GetState.Run(value));

	private void OnInteract() => _playerHandler.GetState.Interact();
}
