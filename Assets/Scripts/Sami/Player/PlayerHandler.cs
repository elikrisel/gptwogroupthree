using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using Isvak.StateMachine;
using UnityEngine;
using TMPro;
using Unity.Mathematics;
using Vehicle = Isvak.StateMachine.Vehicle;
using UnityEngine.UI;
using DG.Tweening;

namespace Isvak.Player
{
	[RequireComponent(typeof(Rigidbody))]
	public class PlayerHandler : FSM
	{
		#region Variables
		
		[Header("Player settings")]
		private Rigidbody _rigidbody;
		public Rigidbody Rigidbody => _rigidbody;

		private float _distanceToGround;
		[HideInInspector] public float DistanceToGround => _distanceToGround;

		private float jumpForce;
		protected float JumpForce => jumpForce;
		
		private float speed;
		public float Speed => speed;
		
		private float runModifier;
		public float RunModifier => runModifier;
		
		private Camera mainCamera;

		[HideInInspector] public Transform currentLadder = null;

		[HideInInspector] public Camera MainCamera => mainCamera;

		[HideInInspector] public float MAXVelocityChange { get; } = 10.0f;

		public Transform head;
		[HideInInspector] public SphereCollider headCollider;
		
		public GameObject stepCheck;
		public GameObject stepCheckCopy;
		[HideInInspector] public float playerWidth;
		
		[HideInInspector] public List<Vector3> directions = new List<Vector3>();
		
		// Not used, but might be used soon
		private TextMeshProUGUI currentState;

		private Vector3 movementDelta;

		public Vector3 MovementDelta => movementDelta;

		public bool isAlive = true;
		public CanvasGroup deathCanvas;
		public Button retryButton;

		public VehicleController vehicle;

		private bool onVehicle = false;

		#endregion

		private void OnEnable()
		{
			EventHandler.Movement += EventHandlerOnMovement;
		}

		private void OnDisable()
		{
			EventHandler.Movement -= EventHandlerOnMovement;
		}

		private void EventHandlerOnMovement(Vector3 delta) => movementDelta = delta;

		private void Awake()
		{
			_rigidbody = GetComponent<Rigidbody>();
			_distanceToGround = GetComponent<CapsuleCollider>().bounds.extents.y;
			mainCamera = Camera.main;
		}

		/*
		 * UNITY
		 */
		public virtual void Start()
		{
			/*
			 * Setting up the player
			 */
			jumpForce = GameManager.Instance.PlayerSettings.JumpForce;
			speed = GameManager.Instance.PlayerSettings.Speed;
			runModifier = GameManager.Instance.PlayerSettings.SpeedModifier;
			
			stepCheckCopy = Instantiate(
				stepCheck, 
				new Vector3(
					stepCheck.transform.position.x, 
					stepCheck.transform.position.y + GameManager.Instance.PlayerSettings.StepHeight,
					stepCheck.transform.position.z), 
				Quaternion.identity);
			stepCheckCopy.transform.SetParent(this.gameObject.transform);
			
			/*
			 * Set player width
			 */
			playerWidth = GetComponent<Renderer>().bounds.size.x / 2;
			
			/*
			 * Setup directions
			 */
			float gizmoLength = playerWidth + 1.5f;

			directions.Add(transform.TransformDirection(Vector3.forward) * gizmoLength);
			directions.Add(transform.TransformDirection((transform.forward + transform.right).normalized) * gizmoLength);
			directions.Add(transform.TransformDirection(Vector3.left) * gizmoLength);
			directions.Add(transform.TransformDirection((-transform.forward - -transform.right).normalized) * gizmoLength);
			
			directions.Add(transform.TransformDirection(-Vector3.forward) * gizmoLength);
			directions.Add(transform.TransformDirection((-transform.forward + -transform.right).normalized) * gizmoLength);
			directions.Add(transform.TransformDirection((transform.forward - transform.right).normalized) * gizmoLength);
			directions.Add(transform.TransformDirection(-Vector3.left) * gizmoLength);
			
			/*
			 * NOTE TO DEVELOPER:
			 * More terrible code to follow, but I'm sure it's good enough for the prototype
			 */
			headCollider = head.GetComponent<SphereCollider>();
			
			
			SetState(new Idle(this));
		}

		private void TriggerDeath()
		{
			deathCanvas.DOFade(1.0f, 1.0f)
				.OnComplete(() => {
					retryButton.interactable = true;
					deathCanvas.interactable = true;
					deathCanvas.blocksRaycasts = true;

					Cursor.lockState = CursorLockMode.None;
					Cursor.visible = true;

					Time.timeScale = 0.0f;
				});
		}

		private void Update()
		{
			if (!isAlive)
				return;

			if (PlayerStats.CurrentHealth <= 0.0f)
			{
				isAlive = false;
				TriggerDeath();
			}

			if (GetState.ToString() == "Isvak.StateMachine.Climb")
				return;

			Ray ray = new Ray(mainCamera.transform.position, mainCamera.transform.forward);
			RaycastHit hit;
			
			if (Physics.Raycast(ray, out hit, 3.5f))
			{
				Vector3 forward = mainCamera.transform.TransformDirection(Vector3.forward) * 3.5f;
				Debug.DrawRay(mainCamera.transform.position, forward, Color.yellow);

				if (hit.transform.gameObject.CompareTag("Vehicle") && GetState.ToString() != "Isvak.StateMachine.Vehicle")
				{
					if (Input.GetKeyDown("e"))
					{
						if (onVehicle)
							return;

						onVehicle = true;

						vehicle = hit.transform.GetComponent<VehicleController>();

						_rigidbody.useGravity = false;
						_rigidbody.isKinematic = true;
						GetComponent<CapsuleCollider>().isTrigger = true;
						transform.SetParent(vehicle.seat);
						SetState(new Vehicle(this));
					}
				}
				
				if (hit.transform.gameObject.CompareTag("Ladder") && GetState.ToString() == "Isvak.StateMachine.Swim")
				{
					if (Input.GetKeyDown("e"))
					{
						currentLadder = hit.transform;
						
						_rigidbody.useGravity = false;
						_rigidbody.isKinematic = true;
						GetComponent<CapsuleCollider>().isTrigger = true;

						head.GetComponent<Rigidbody>().useGravity = false;
						head.GetComponent<Rigidbody>().isKinematic = true;
						headCollider.isTrigger = true;

						 Vector3 startPosition = hit.transform.localPosition + hit.transform.right * 3.25f;
						 startPosition.y = transform.position.y;

						 transform.position = startPosition;
						
						SetState(new Climb(this));
					}
				}
			}
		}

		public void ResetPlayer()
		{
			SetState(new Walk(this));
			transform.parent = null;

			_rigidbody.useGravity = true;
			_rigidbody.isKinematic = false; 
			GetComponent<CapsuleCollider>().isTrigger = false;
			
			head.GetComponent<Rigidbody>().useGravity = true;
			head.GetComponent<Rigidbody>().isKinematic = false; 
			headCollider.isTrigger = false;

			vehicle = null;

			StartCoroutine(ActivateVehicle());
		}

		public IEnumerator ActivateVehicle()
		{
			yield return new WaitForSeconds(1);
			onVehicle = false;
		}

		private void FixedUpdate()
		{
			State.OnFixedUpdate();
		}
	}

}