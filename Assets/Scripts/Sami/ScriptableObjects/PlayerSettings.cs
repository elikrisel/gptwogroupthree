using UnityEngine;

namespace Isvak.Player.Settings
{
    [CreateAssetMenu(menuName = "Isvak/Player Settings")]
    public class PlayerSettings : ScriptableObject
    {
        #region Variables
        
        // Jump force
        [SerializeField] private float jumpForce;
        public float JumpForce => jumpForce;
        
        // Player speed
        [SerializeField] private float speed;
        public float Speed => speed;
        
        // Player walk speed
        [SerializeField] private float speedModifier;
        public float SpeedModifier => speedModifier;
        
        // Player step height
        [SerializeField] private float stepHeight;
        public float StepHeight => stepHeight;
        
        // Player step smoothing
        [SerializeField] private float stepSmooth;

        public float StepSmooth => stepSmooth;

        #endregion
    }
   
}