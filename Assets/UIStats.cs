using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIStats : MonoBehaviour
{
    #region Variables

    [SerializeField] private Image playerOxygen;
    [SerializeField] private Image playerHealth;
    [SerializeField] private Image playerTemperature;

    #endregion

    void Update()
    {
        playerOxygen.fillAmount = PlayerStats.CurrentOxygen / PlayerStats.BaseOxygen;
        playerHealth.fillAmount = PlayerStats.CurrentHealth / PlayerStats.BaseHealth;
        playerTemperature.fillAmount = PlayerStats.CurrentTemperature / PlayerStats.BaseTemperature;
    }
}
