using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class LadderTest : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {

        Vector3 offset = GetComponent<BoxCollider>().bounds.center;
        float height = GetComponent<BoxCollider>().bounds.size.y;

        float top = (height / 2) + offset.y;

        //Vector3 offset = transform.up * (transform.localScale.y / 2);
        //Vector3 pos = transform.position + offset; //This is the position

        Debug.Log($"Top: {top}");
    }
}
