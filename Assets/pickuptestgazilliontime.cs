using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pickuptestgazilliontime : MonoBehaviour
{
   public float pickupRange = 5f;
   public float moveForce = 250f;
   private GameObject heldObj;
   public Transform holdParent;
   
   void Update()
   {
      if (Input.GetButtonDown("PickUp"))
      {
         if (heldObj == null)
         {
            
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, pickupRange))
            {
            
               PickUpObject(hit.transform.gameObject);      
            
            }
   
            
            
         }
         else
         {
            
            DropObject();

         }
         

      }

      if (heldObj != null)
      {

         MoveObject();

      }
      
   }

   void MoveObject()
   {

      if (Vector3.Distance(heldObj.transform.position, holdParent.position) > 0.1f)
      {
         Vector3 moveDirection = (holdParent.position - heldObj.transform.position);
         heldObj.GetComponent<Rigidbody>().AddForce(moveDirection * moveForce);
         
      }
      
      
   }
   void PickUpObject(GameObject pickUp)
   {

      if (pickUp.GetComponent<Rigidbody>())
      {
         Rigidbody rb = pickUp.GetComponent<Rigidbody>();
         rb.useGravity = false;
         rb.drag = 10;
         rb.transform.parent = holdParent;

         heldObj = pickUp;
      }
      
   }


   void DropObject()
   {
      Rigidbody heldRig = heldObj.GetComponent<Rigidbody>();
      heldRig.useGravity = true;
      heldRig.drag = 1;

      heldObj.transform.parent = null;
      heldObj = null;


   }

}
